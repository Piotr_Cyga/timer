package pl.sda.timer;

import java.util.List;

public interface IHistory {

    List<Logs> getListOfLogs();
}
