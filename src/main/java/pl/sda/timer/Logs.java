package pl.sda.timer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Logs {
    private LocalDateTime time;
    private Action action;


    public Logs() {
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
