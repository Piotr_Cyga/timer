package pl.sda.timer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;


@Controller
public class TimerController {
    History history = new History();

    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public ModelAndView startCountdown () {
    Logs logs = new Logs();
    logs.setTime(LocalDateTime.now());
    logs.setAction(Action.START);
    history.getListOfLogs().add(logs);
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("startTime", logs.getTime());
        return modelAndView;
}
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public ModelAndView init () {
        Logs logs = new Logs();
        logs.setTime(LocalDateTime.now());
        logs.setAction(Action.START);
        history.getListOfLogs().add(logs);
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("startTime", logs.getTime());
        return modelAndView;
    }
}
